﻿using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class OtherContact 
    {
        public int Id { get; set; }

        [Required]
        public string Value { get; set; }

        public virtual Contact Contact { get; set; }
    }
}