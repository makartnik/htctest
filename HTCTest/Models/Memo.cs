﻿namespace HTCTest.Models
{
    public class Memo : Note
    {
        public override NoteType Type => NoteType.Memo;
    }
}