﻿using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class Phone
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Number { get; set; }

        public virtual Contact Contact { get; set; }
    }
}