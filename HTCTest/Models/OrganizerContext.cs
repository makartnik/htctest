﻿using System.Data.Entity;

namespace HTCTest.Models
{
    public class OrganizerContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public DbSet<Note> Notes { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Matter>();
            modelBuilder.Entity<Meeting>();
            modelBuilder.Entity<Memo>();
            modelBuilder.Entity<Contact>();
            modelBuilder.Entity<Phone>().HasRequired(p => p.Contact).WithMany(c => c.Phones);
            modelBuilder.Entity<EMail>().HasRequired(e => e.Contact).WithMany(c => c.EMails);
            modelBuilder.Entity<Skype>().HasRequired(s => s.Contact).WithMany(c => c.Skypes);
            modelBuilder.Entity<OtherContact>().HasRequired(oc => oc.Contact).WithMany(c => c.OtherContacts);
        }

        public System.Data.Entity.DbSet<HTCTest.Models.Memo> Memos { get; set; }

        public System.Data.Entity.DbSet<HTCTest.Models.Matter> Matters { get; set; }

        public System.Data.Entity.DbSet<HTCTest.Models.Meeting> Meetings { get; set; }
    }
}