﻿using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class EMail
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Address { get; set; }

        public virtual Contact Contact { get; set; }
    }
}