﻿namespace HTCTest.Models
{
    public enum NoteType
    {
        Meeting,
        Matter,
        Memo
    }
}