﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public abstract class Note
    {
        public int Id { get; set; }

        public abstract NoteType Type { get; }

        public string Topic { get; set; }

        [Display(Name = "Start date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Display(Name = "Done")]
        public bool IsDone { get; set; }
    }
}