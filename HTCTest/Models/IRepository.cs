﻿using System;
using System.Linq;

namespace HTCTest.Models
{
    public interface IRepository<T> :IDisposable
        where T : class
    {
        IQueryable<T> GetAll();

        T Get(int id);

        void Add(T item);

        void Remove(int id);

        void Update(T item);
    }
}