﻿using System;
using System.Data.Entity;
using System.Linq;

namespace HTCTest.Models
{
    public class NotesRepository : IRepository<Note>
    {
        private readonly OrganizerContext _context = new OrganizerContext();

        public IQueryable<Note> GetAll()
        {
            return _context.Notes;
        }

        public Note Get(int id)
        {
            return _context.Notes.Find(id);
        }

        public void Add(Note item)
        {
            _context.Notes.Add(item);
            _context.SaveChanges();
        }

        public void Remove(int id)
        {
            var note = Get(id);
            if (note == null)
                throw new InvalidOperationException();
            _context.Notes.Remove(note);
            _context.SaveChanges();
        }

        public void Update(Note item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}