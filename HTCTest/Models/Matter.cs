﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class Matter : Note
    {
        public override NoteType Type => NoteType.Matter;

        [Display(Name = "Finish date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FinishDate { get; set; }
    }
}