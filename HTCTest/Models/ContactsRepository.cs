﻿using System;
using System.Linq;
using WebGrease.Css.Extensions;

namespace HTCTest.Models
{
    public class ContactsRepository : IRepository<Contact>
    {
        private readonly OrganizerContext _context = new OrganizerContext();

        public void Dispose()
        {
            _context.Dispose();
        }

        public IQueryable<Contact> GetAll()
        {
            return _context.Contacts;
        }

        public Contact Get(int id)
        {
            return _context.Contacts.Find(id);
        }

        public void Add(Contact item)
        {
            _context.Contacts.Add(item);
            _context.SaveChanges();
        }

        public void Remove(int id)
        {
            var contact = Get(id);
            if (contact == null)
                throw new InvalidOperationException();
            _context.Contacts.Remove(contact);
            _context.SaveChanges();
        }

        public void Update(Contact item)
        {
            var existingItem = _context.Contacts.Find(item.Id);
            if (existingItem == null)
                throw new InvalidOperationException();

            foreach (var phone in existingItem.Phones.ToList())
                if (item.Phones.All(p => p.Id != phone.Id))
                    _context.Set<Phone>().Remove(phone);
            foreach (var email in existingItem.EMails.ToList())
                if (item.EMails.All(e => e.Id != email.Id))
                    _context.Set<EMail>().Remove(email);
            foreach (var skype in existingItem.Skypes.ToList())
                if (item.Skypes.All(s => s.Id != skype.Id))
                    _context.Set<Skype>().Remove(skype);
            foreach (var other in existingItem.OtherContacts.ToList())
                if (item.OtherContacts.All(o => o.Id != other.Id))
                    _context.Set<OtherContact>().Remove(other);

            var phones = existingItem.Phones;
            item.Phones.Where(p => p.Id == 0).ForEach(p => phones.Add(p));
            item.Phones.Where(p => p.Id != 0).ForEach(p =>
            {
                var existing = _context.Set<Phone>().Single(e => e.Id == p.Id);
                _context.Entry(existing).CurrentValues.SetValues(p);
            });
            var emalis = existingItem.EMails;
            item.EMails.Where(p => p.Id == 0).ForEach(p => emalis.Add(p));
            item.EMails.Where(p => p.Id != 0).ForEach(p =>
            {
                var existing = _context.Set<EMail>().Single(e => e.Id == p.Id);
                _context.Entry(existing).CurrentValues.SetValues(p);
            });
            var skypes = existingItem.Skypes;
            item.Skypes.Where(p => p.Id == 0).ForEach(p => skypes.Add(p));
            item.Skypes.Where(p => p.Id != 0).ForEach(p =>
            {
                var existing = _context.Set<Skype>().Single(e => e.Id == p.Id);
                _context.Entry(existing).CurrentValues.SetValues(p);
            });
            var others = existingItem.OtherContacts;
            item.OtherContacts.Where(p => p.Id == 0).ForEach(p => others.Add(p));
            item.OtherContacts.Where(p => p.Id != 0).ForEach(p =>
            {
                var existing = _context.Set<OtherContact>().Single(e => e.Id == p.Id);
                _context.Entry(existing).CurrentValues.SetValues(p);
            });


            _context.Entry(existingItem).CurrentValues.SetValues(item);

            _context.SaveChanges();
        }
    }
}