﻿using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class Skype
    {
        public int Id { get; set; }

        [Required]
        public string Login { get; set; }

        public virtual Contact Contact { get; set; }
    }
}