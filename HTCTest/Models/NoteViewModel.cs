﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class NoteViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Done")]
        public bool IsDone { get; set; }

        public NoteType Type { get; set; }

        [Required]
        public string Topic { get; set; }

        [Required]
        [Display(Name = "Start date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Display(Name = "Finish date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? FinishDate { get; set; }

        public string Place { get; set; }
    }
}