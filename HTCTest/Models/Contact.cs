﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class Contact
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        public string Patronymic { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }

        public string Firm { get; set; }

        public string Rank { get; set; }

        public virtual ICollection<Phone> Phones { get; set; }

        [Display(Name = "E-mails")]
        public virtual ICollection<EMail> EMails { get; set; }

        public virtual ICollection<Skype> Skypes { get; set; }

        [Display(Name = "Other contacts")]
        public virtual ICollection<OtherContact> OtherContacts { get; set; }

        public Contact()
        {
            Phones = new List<Phone>();
            EMails = new List<EMail>();
            Skypes = new List<Skype>();
            OtherContacts = new List<OtherContact>();
        }
    }
}