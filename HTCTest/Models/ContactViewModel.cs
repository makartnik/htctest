﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class ContactViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        public string Patronymic { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }

        public string Firm { get; set; }

        public string Rank { get; set; }

        public IList<Phone> Phones { get; set; }

        public IList<EMail> EMails { get; set; }

        public IList<Skype> Skypes { get; set; }

        public IList<OtherContact> OtherContacts { get; set; }

        public ContactViewModel()
        {
            Phones = new List<Phone>();
            EMails = new List<EMail>();
            Skypes = new List<Skype>();
            OtherContacts = new List<OtherContact>();
        }
    }
}