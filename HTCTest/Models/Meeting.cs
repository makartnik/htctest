﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HTCTest.Models
{
    public class Meeting : Note
    {
        public override NoteType Type => NoteType.Meeting;

        [Display(Name = "Finish date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FinishDate { get; set; }

        public string Place { get; set; }
    }
}