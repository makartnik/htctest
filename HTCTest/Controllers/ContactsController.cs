﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using HTCTest.Models;

namespace HTCTest.Controllers
{
    public class ContactsController : Controller
    {
        private readonly IRepository<Contact> _repository = new ContactsRepository();

        public ActionResult Index(string searchString)
        {
            ViewBag.SearchString = "";
            if (string.IsNullOrEmpty(searchString))
                return View(_repository.GetAll().ToList());
            DateTime date;
            bool isDate = DateTime.TryParseExact(searchString, "yyyy-MM-dd", DateTimeFormatInfo.CurrentInfo,
                DateTimeStyles.None, out date);

            ViewBag.SearchString = searchString;
            var searchResult = _repository.GetAll().Where(c =>
                    c.FirstName.Equals(searchString) ||
                    c.LastName.Equals(searchString) ||
                    c.Patronymic.Equals(searchString) ||
                    isDate && c.Birthdate.Equals(date) ||
                    c.Phones.Select(p => p.Number).Contains(searchString) ||
                    c.EMails.Select(e => e.Address).Contains(searchString) ||
                    c.Skypes.Select(s => s.Login).Contains(searchString) ||
                    c.OtherContacts.Select(oc => oc.Value).Contains(searchString) ||
                    c.Firm.Equals(searchString) ||
                    c.Rank.Equals(searchString))
                .ToList();
            return View(searchResult);
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = _repository.Get(id.Value);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Id,FirstName,LastName,Patronymic,Birthdate,Firm,Rank,Phones,EMails,Skypes,OtherContacts")]
            ContactViewModel contactViewModel)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(cfg => cfg.CreateMap<ContactViewModel, Contact>());
                var contact = Mapper.Map<ContactViewModel, Contact>(contactViewModel);
                _repository.Add(contact);
                return RedirectToAction("Index");
            }
            return View(contactViewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = _repository.Get(id.Value);
            if (contact == null)
            {
                return HttpNotFound();
            }
            Mapper.Initialize(cfg => cfg.CreateMap<Contact, ContactViewModel>()
                .ForMember("Phones", opt => opt.MapFrom(src => src.Phones.ToList()))
                .ForMember("EMails", opt => opt.MapFrom(src => src.EMails.ToList()))
                .ForMember("Skypes", opt => opt.MapFrom(src => src.Skypes.ToList()))
                .ForMember("OtherContacts", opt => opt.MapFrom(src => src.OtherContacts.ToList())));
            var contactViewModel = Mapper.Map<Contact, ContactViewModel>(contact);
            return View(contactViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "Id,FirstName,LastName,Patronymic,Birthdate,Firm,Rank,Phones,EMails,Skypes,OtherContacts")]
            ContactViewModel contactViewModel)
        {
            if (ModelState.IsValid)
            {
                Mapper.Initialize(cfg => cfg.CreateMap<ContactViewModel, Contact>());
                var contact = Mapper.Map<ContactViewModel, Contact>(contactViewModel);
               _repository.Update(contact);
                return RedirectToAction("Index");
            }
            return View(contactViewModel);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = _repository.Get(id.Value);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return PartialView(contact);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = _repository.Get(id);
            if (contact == null)
                return HttpNotFound();
            _repository.Remove(id);


            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}