﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using HTCTest.Models;

namespace HTCTest.Controllers
{
    public class NotesController : Controller
    {
        private readonly IRepository<Note> _repository = new NotesRepository();

        public ActionResult Index()
        {
            return RedirectToAction("All", DateTime.Today);
        }

        public ActionResult ByDays(DateTime date)
        {
            var notes = _repository.GetAll().Where(n => n.StartDate.Equals(date)).ToList();
            ViewBag.Date = date;
            return View(notes);
        }

        public ActionResult ByWeeks(DateTime date)
        {
            DateTime weekStart = date.AddDays(-(int) date.DayOfWeek);
            DateTime weekEnd = weekStart.AddDays(7);
            var notes = _repository.GetAll().Where(n => n.StartDate.CompareTo(weekStart) >= 0 &&
                                                        n.StartDate.CompareTo(weekEnd) < 0)
                .ToList();
            ViewBag.WeekStart = weekStart;
            ViewBag.WeekEnd = weekEnd.AddDays(-1);
            ViewBag.Date = date;
            return View(notes);
        }

        public ActionResult ByMonths(DateTime date)
        {
            var notes = _repository.GetAll().Where(n => n.StartDate.Month == date.Month &&
                                                        n.StartDate.Year == date.Year)
                .ToList();
            ViewBag.Date = date;
            return View(notes);
        }

        public ActionResult All(DateTime date)
        {
            ViewBag.Date = date;
            return View(_repository.GetAll().ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = _repository.Get(id.Value);
            if (note == null)
            {
                return HttpNotFound();
            }
            NoteViewModel noteView = new NoteViewModel
            {
                Id = note.Id,
                IsDone = note.IsDone,
                Type = note.Type,
                Topic = note.Topic,
                StartDate = note.StartDate,
                FinishDate = note.Type == NoteType.Matter
                    ? ((Matter) note).FinishDate
                    : note.Type == NoteType.Meeting
                        ? (DateTime?) ((Meeting) note).FinishDate
                        : null,
                Place = note.Type == NoteType.Meeting ? ((Meeting) note).Place : null
            };
            return View(noteView);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Id,Type,Topic,StartDate,FinishDate,Place,IsDone")] NoteViewModel noteView)
        {
            ValidateNoteView(noteView);

            if (ModelState.IsValid)
            {
                Note note;
                switch (noteView.Type)
                {
                    case NoteType.Matter:
                        Mapper.Initialize(cfg => cfg.CreateMap<NoteViewModel, Matter>()
                            .ForMember("FinishDate", opt => opt.MapFrom(src => src.FinishDate.Value)));
                        note = Mapper.Map<NoteViewModel, Matter>(noteView);
                        break;
                    case NoteType.Meeting:
                        Mapper.Initialize(cfg => cfg.CreateMap<NoteViewModel, Meeting>()
                            .ForMember("FinishDate", opt => opt.MapFrom(src => src.FinishDate.Value)));
                        note = Mapper.Map<NoteViewModel, Meeting>(noteView);
                        break;
                    case NoteType.Memo:
                        Mapper.Initialize(cfg => cfg.CreateMap<NoteViewModel, Memo>());
                        note = Mapper.Map<NoteViewModel, Memo>(noteView);
                        break;
                    default:
                        throw new InvalidEnumArgumentException(nameof(noteView.Type));
                }
                _repository.Add(note);

                return RedirectToAction("Index");
            }
            return View(noteView);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = _repository.Get(id.Value);
            if (note == null)
            {
                return HttpNotFound();
            }
            Mapper.Initialize(cfg => cfg.CreateMap<Note, NoteViewModel>()
                .ForMember("FinishDate", opt => opt.MapFrom(src => src.Type == NoteType.Matter
                    ? ((Matter) src).FinishDate
                    : src.Type == NoteType.Meeting
                        ? (DateTime?) ((Meeting) src).FinishDate
                        : null))
                .ForMember("Place", opt => opt.MapFrom(src => src.Type == NoteType.Meeting
                    ? ((Meeting) src).Place
                    : null)));
            var noteView = Mapper.Map<Note, NoteViewModel>(note);
            return View("Edit", noteView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "Id,Type,Topic,StartDate,FinishDate,Place,IsDone")] NoteViewModel noteView)
        {
            ValidateNoteView(noteView);

            if (ModelState.IsValid)
            {
                Note note;
                switch (noteView.Type)
                {
                    case NoteType.Matter:
                        Mapper.Initialize(cfg => cfg.CreateMap<NoteViewModel, Matter>()
                            .ForMember("FinishDate", opt => opt.MapFrom(src => src.FinishDate.Value)));
                        note = Mapper.Map<NoteViewModel, Matter>(noteView);
                        break;
                    case NoteType.Meeting:
                        Mapper.Initialize(cfg => cfg.CreateMap<NoteViewModel, Meeting>()
                            .ForMember("FinishDate", opt => opt.MapFrom(src => src.FinishDate.Value)));
                        note = Mapper.Map<NoteViewModel, Meeting>(noteView);
                        break;
                    case NoteType.Memo:
                        Mapper.Initialize(cfg => cfg.CreateMap<NoteViewModel, Memo>());
                        note = Mapper.Map<NoteViewModel, Memo>(noteView);
                        break;
                    default:
                        throw new InvalidEnumArgumentException(nameof(noteView.Type));
                }
                _repository.Update(note);
                return RedirectToAction("Index");
            }
            return View(noteView);
        }

        private void ValidateNoteView(NoteViewModel noteView)
        {
            if (noteView.FinishDate == null && (noteView.Type.Equals(NoteType.Matter) ||
                                                noteView.Type.Equals(NoteType.Meeting)))
                ModelState.AddModelError("FinishDate", "Finish date is requried.");
            if ((noteView.Type.Equals(NoteType.Matter) || noteView.Type.Equals(NoteType.Meeting))
                && noteView.FinishDate != null &&
                noteView.StartDate.CompareTo(noteView.FinishDate.Value) > 0)
            {
                ModelState.AddModelError("StartDate", "Start date must be no more than finish date.");
                ModelState.AddModelError("FinishDate", "Finish date must be at least start date.");
            }

            if (noteView.Type == NoteType.Meeting && string.IsNullOrWhiteSpace(noteView.Place))
                ModelState.AddModelError("Place", "Place is required.");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = _repository.Get(id.Value);
            if (note == null)
            {
                return HttpNotFound();
            }
            return PartialView(note);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Note note = _repository.Get(id);
            if (note == null)
                return HttpNotFound();
            _repository.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}